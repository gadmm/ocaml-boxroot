/* SPDX-License-Identifier: MIT */
#ifndef BOXROOT_HPP
#define BOXROOT_HPP

#include <boxroot.h>

#include <memory>

/* This defines a movable smart pointer Boxroot based on
   std::unique_ptr. */

struct BoxrootDeleter {
  void operator()(boxroot b) const noexcept { boxroot_delete(b); }
};

using boxroot_unique_ptr = std::unique_ptr<struct bxr_private, BoxrootDeleter>;

[[noreturn]] void boxroot_raise_error();

static inline boxroot bxr_create_exn(value v)
{
  boxroot b = boxroot_create(v);
  if (BXR_UNLIKELY(b == NULL)) boxroot_raise_error();
  return b;
}

class Boxroot {
  boxroot_unique_ptr ptr_;
public:
  explicit Boxroot(value x) : ptr_(bxr_create_exn(x)) {}

  value const & operator*() const noexcept
  {
    return *boxroot_get_ref(ptr_.get());
  }

  void set(value x)
  {
    if (BXR_UNLIKELY(!boxroot_modify(reinterpret_cast<boxroot *>(&ptr_), x))) {
      boxroot_raise_error();
    }
    /* More legal but poor codegen: */
    /*
      boxroot b = _ptr.release();
      bool res = boxroot_modify(&b, x);
      _ptr.reset(b);
      if (BXR_UNLIKELY(!res)) boxroot_raise_error();
    */
  }
};

#endif // BOXROOT_HPP
