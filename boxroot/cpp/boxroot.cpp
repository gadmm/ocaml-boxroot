/* SPDX-License-Identifier: MIT */
#include "boxroot.h"

#include <stdexcept>

/* Assumes that a call to boxroot_create or boxroot_modify just failed. */
[[noreturn]] void boxroot_raise_error()
{
  std::string str;
  str += "Boxroot error: ";
  str += boxroot_error_string();
  str += ".";
  throw std::runtime_error(str);
}
