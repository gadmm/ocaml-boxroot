(* SPDX-License-Identifier: MIT *)
module Choice_config = Choice.Config
module Choice = Choice_config.Choice
open Choice.Syntax

let rec insert : type a . a -> a list -> a list Choice.t =
  fun elt xs -> match xs with
  | [] -> Choice.return [elt]
  | x :: xs ->
    Choice.choice
      (Choice.return (elt :: x :: xs))
      (let+ xs' = insert elt xs in x :: xs')

let rec permutation : type a . a list -> a list Choice.t = function
  | [] -> Choice.return []
  | x :: xs ->
    let* xs' = permutation xs in
    insert x xs'

(* (range n) is [0; ..; n-1] *)
let range n =
  let rec loop acc n =
    if n < 0 then acc
    else loop (n :: acc) (n - 1)
  in loop [] (n - 1)

let debug = false

(* the number could be large, so count it as an int64 *)
let count_permutations n =
  let input = range n in
  let perm = permutation input in
  Choice.run perm
    (fun acc li ->
       if debug then begin
         List.iter (Printf.printf "%d ") li; print_newline ();
       end;
       Int64.succ acc
    ) 0L

let get_int_var var =
  try int_of_string (Sys.getenv var)
  with _ ->
    Printf.ksprintf failwith
      "We expected an environment variable %s with an integer value."
      var

let n = get_int_var "N"
let niters = get_int_var "NITERS"
let doms = get_int_var "DOMS"


type scheduling = Domains | Threads | Sequential
let scheduling : scheduling =
  match Sys.getenv "SCHEDULING" with
  | "domains" -> Domains
  | "threads" -> Threads
  | "sequential" -> Sequential
  | _ | exception _ ->
    Printf.ksprintf failwith
      "Missing or incorrect value the environment variable SCHEDULING; \
       expected [domains | threads | sequential]"

let rec fact n = if n = 0 then 1L else Int64.(mul (of_int n) (fact (n - 1)))

module type Scheduler = sig
  type 'a t
  val spawn : (unit -> 'a) -> 'a t
  val join : 'a t -> 'a
end

module DomainsScheduler : Scheduler = Domain

module ThreadsScheduler : Scheduler = struct
  type 'a t = Thread.t * ('a, exn * Printexc.raw_backtrace) result option ref
  let spawn f =
    let result = ref None in
    let t =
      Thread.create (fun out ->
        out := Some (match f () with
          | v -> Ok v
          | exception exn ->
            let bt = Printexc.get_raw_backtrace () in
            Error (exn, bt)
        )
      ) result
    in
    (t, result)

  let join (t, result) =
    Thread.join t;
    match !result with
    | None -> assert false
    | Some (Ok v) -> v
    | Some (Error (exn, bt)) ->
      Printexc.raise_with_backtrace exn bt
end

module SequentialScheduler : Scheduler = struct
  type 'a t = 'a Lazy.t
  let spawn f = lazy (f ())
  let join t = Lazy.force t
end

let () =
  Ref.Config.Ref.setup ();
  Printf.printf "%-*s N=%-2d DOMS=%-3d NITERS=%-8d: %!"
    Ref.Config.max_implem_name_length Ref.Config.implem_name n doms niters;
  let before_times = Unix.times () in
  let before = Time.time () in
  let do_count () =
    let count = count_permutations n in
    for _ = 1 to niters - 1 do
      let count' = count_permutations n in
      assert (count = count');
    done;
    count
  in
  let (module Sched : Scheduler) =
    match scheduling with
    | Domains -> (module DomainsScheduler)
    | Threads -> (module ThreadsScheduler)
    | Sequential -> (module SequentialScheduler)
  in
  let worker_domains = List.init (doms - 1) (fun _ -> Sched.spawn do_count) in
  let count = do_count () in
  let counts = count :: List.map Sched.join worker_domains in
  let after = Time.time () in
  let real_time = after -. before in
  let after_times = Unix.times () in
  let user_time = Unix.(after_times.tms_utime -. before_times.tms_utime) in
  let system_time = Unix.(after_times.tms_stime -. before_times.tms_stime) in
  Printf.printf "%.2fs (user time %.2fs, %.2fs/dom%t)\n%!"
    real_time user_time (user_time /. float doms)
    (fun _ ->
       let system_ratio = system_time /. (system_time +. user_time) in
       if system_ratio < 0.01 then ()
       else
         Printf.printf "; system time %.2fs, %.2fs/dom, %.2f%%"
           system_time (system_time /. float doms) (system_ratio *. 100.)
    );
  assert (List.for_all ((=) (fact n)) counts);
  ignore (Sys.opaque_identity count);
  if Ref.Config.show_stats then
    Ref.Config.Ref.print_stats ();
  Ref.Config.Ref.teardown ();
